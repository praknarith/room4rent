//
//  SignupViewController.h
//  RentRoom
//
//  Created by Narith on 9/18/16.
//  Copyright © 2016 praknarith. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface SignupViewController : UIViewController
- (IBAction)btnSignUp:(id)sender;
- (IBAction)btnCancel:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *textUser;
@property (weak, nonatomic) IBOutlet UITextField *textSignUpEmail;
@property (weak, nonatomic) IBOutlet UITextField *textSignupPassword;


@end
