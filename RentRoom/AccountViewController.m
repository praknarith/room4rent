//
//  AccountViewController.m
//  RentRoom
//
//  Created by Narith on 10/10/16.
//  Copyright © 2016 praknarith. All rights reserved.
//

#import "AccountViewController.h"
#import "SingInViewController.h"
#import "AppState.h"
#import <FirebaseAuth/FIRUserInfo.h>
#import <UIKit/UIView.h>
#import <FBSDKCoreKit/FBSDKProfilePictureView.h>
@import FirebaseAuth;
@interface AccountViewController ()

@property (nonatomic, strong, readonly) FBSDKProfilePictureView *profilePictureView;

- (IBAction)logOutButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *addressTextField;
- (IBAction)phoneEdit:(id)sender;
- (IBAction)passwordEdit:(id)sender;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *loginLogout;

@end

@implementation AccountViewController{
    FBSDKProfilePictureView *_profilePictureView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    FIRUser *user = [FIRAuth auth].currentUser;
    if (!user) {
        [self.loginLogout setTitle:@"Log in"];
    }
    else{
        NSString *name = user.displayName;
//        NSString *phone = user.phone;
//        NSString *email = user.email;
        NSURL *photoUrl = user.photoURL;
//        NSString *uid = user.uid;
        
        self.nameTextField.text=name;
        self.profileImage.image=[UIImage imageWithData:[NSData dataWithContentsOfURL:photoUrl]];
        self.profileImage.layer.cornerRadius=35;
        self.profileImage.clipsToBounds=YES;
        
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)logOutButton:(id)sender {
    NSError *error;
    [[FIRAuth auth] signOut:&error];
    if (!error) {
        [AppState sharedInstance].signedIn = false;
        [self performSegueWithIdentifier:@"home" sender:nil];
    }
    else {
        NSLog(@"Error signing out: %@", error);
        return;
    }
   [FBSDKAccessToken setCurrentAccessToken:nil];
}
- (IBAction)phoneEdit:(id)sender {
}

- (IBAction)passwordEdit:(id)sender {
}


@end
