//
//  RoomTableViewCell.m
//  RentRoom
//
//  Created by Narith on 9/25/16.
//  Copyright © 2016 praknarith. All rights reserved.
//

#import "RoomTableViewCell.h"

@implementation RoomTableViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    return [super initWithFrame:frame];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
@end
