//
//  ViewController.m
//  RentRoom
//
//  Created by Narith on 9/14/16.
//  Copyright © 2016 praknarith. All rights reserved.
//

#import "SingInViewController.h"
#import "SignupViewController.h"
#import "RoomListViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "AppState.h"
#import "User.h"

@interface SingInViewController ()  <FBSDKLoginButtonDelegate>
@property (weak, nonatomic) IBOutlet FBSDKLoginButton *facebookLoginButton;

@end

@implementation SingInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.textEmail.layer.borderWidth = 2;
    self.textEmail.layer.borderColor = [[UIColor colorWithRed:7.0/255.0 green:199.0/255.0 blue:119.0/255.0 alpha:1.0] CGColor];
    self.textPassword.layer.borderWidth = 2;
    self.textPassword.layer.borderColor = [[UIColor colorWithRed:7.0/255.0 green:199.0/255.0 blue:119.0/255.0 alpha:1.0] CGColor];
    //Set textfield focus
//    [self.textEmail becomeFirstResponder];
    
    //facebook login
    self.facebookLoginButton.delegate=self;
    self.facebookLoginButton.readPermissions =@[@"public_profile", @"email"];
    if([FBSDKAccessToken currentAccessToken]){
        [self performSegueWithIdentifier:@"signIn" sender:nil];
    }
    //Keyboard appear
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidAppear:(BOOL)animated{
    if ([FIRAuth auth].currentUser) {
        [self performSegueWithIdentifier:@"signIn" sender:nil];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBar.hidden = YES;
}

- (IBAction)btnLogin:(id)sender {
    NSString *email = self.textEmail.text;
    NSString *password = self.textPassword.text;
    [[FIRAuth auth] signInWithEmail:email
                           password:password
                         completion:^(FIRUser * _Nullable user, NSError * _Nullable error) {
                             if (error) {
                                 NSLog(@"%@", error.localizedDescription);
                                 return;
                             }
                             [self performSegueWithIdentifier:@"signIn" sender:nil];
                         }];

}



- (void)loginButton:(FBSDKLoginButton *)loginButton didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result error:(NSError *)error {
    if (error == nil) {
        FIRAuthCredential *credential=[FIRFacebookAuthProvider credentialWithAccessToken:[FBSDKAccessToken currentAccessToken].tokenString];
        [[FIRAuth auth] signInWithCredential:credential completion:^(FIRUser * _Nullable user, NSError * _Nullable error) {
            
        }];
    } else {
        NSLog(@"%@", error.localizedDescription);
        
    }
    [self performSegueWithIdentifier:@"signIn" sender:nil];

}

-(void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton{
    NSLog(@"User did logout");
}

-(void)signedIn:(FIRUser *)user{
    [AppState sharedInstance].displayName = user.displayName.length > 0 ? user.displayName : user.email;
    [AppState sharedInstance].photoUrl = user.photoURL;
    [AppState sharedInstance].signedIn = YES;
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:NotificationKeysSignedIn
//                                                        object:nil userInfo:nil];
    [self performSegueWithIdentifier:@"signIn" sender:nil];
}
- (IBAction)btnSignUp:(id)sender {
    UIStoryboard *storyboard =[UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SignupViewController *signupView =[storyboard instantiateViewControllerWithIdentifier:@"SignupViewControll"];
    [self.navigationController presentViewController:signupView animated:YES completion:nil];

//    [self performSegueWithIdentifier:@"signUpView" sender:nil];
}

- (IBAction)btnSkip:(id)sender {
    [self performSegueWithIdentifier:@"signIn" sender:nil];
}

- (void)keyboardDidShow:(NSNotification *)notification
{
    // Assign new frame to view
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    [UIView animateWithDuration:0.1 animations:^{
        CGRect frame = self.view.frame;
        frame.origin.y = -keyboardSize.height;
        self.view.frame = frame;
    }];
}

-(void)keyboardDidHide:(NSNotification *)notification
{
    [UIView animateWithDuration:0.1 animations:^{
        CGRect frame = self.view.frame;
        frame.origin.y = 0.0f;
        self.view.frame = frame;
    }];
}


@end
