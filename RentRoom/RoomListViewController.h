//
//  RoomListViewController.h
//  RentRoom
//
//  Created by Narith on 9/24/16.
//  Copyright © 2016 praknarith. All rights reserved.
//

#import <UIKit/UIKit.h>
@import FirebaseDatabaseUI;
@import Firebase;

@interface RoomListViewController : UIViewController <UITableViewDelegate>
// [START define_database_reference]
@property (strong, nonatomic) FIRDatabaseReference *ref;
// [END define_database_reference]
@property (strong, nonatomic) FirebaseTableViewDataSource *dataSource;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (NSString *) getUid;
@end
