//
//  RoomDetailViewController.h
//  RentRoom
//
//  Created by Narith on 10/11/16.
//  Copyright © 2016 praknarith. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RoomDetailViewController : UIViewController <UITextViewDelegate>
@property (strong, nonatomic) NSString *postKey;
@end
