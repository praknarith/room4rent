//
//  AppState.h
//  RentRoom
//
//  Created by Narith on 10/11/16.
//  Copyright © 2016 praknarith. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
@interface AppState : NSObject
+ (AppState *)sharedInstance;
@property (nonatomic, retain) User *currentUser;

@property (nonatomic) BOOL signedIn;
@property (nonatomic, retain) NSString *displayName;
@property (nonatomic, retain) NSURL *photoUrl;
@end
