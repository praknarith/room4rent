//
//  PostRoomViewController.h
//  RentRoom
//
//  Created by Narith on 10/10/16.
//  Copyright © 2016 praknarith. All rights reserved.
//

#import <UIKit/UIKit.h>
@import Firebase;

@interface PostRoomViewController : UIViewController
- (IBAction)uploadButton:(id)sender;
- (IBAction)postButton:(UIBarButtonItem *)sender;
@property (weak, nonatomic) IBOutlet UIImageView *coverImage;
@property(strong, nonatomic) FIRDatabaseReference *ref;
@end
