//
//  PostRoom.h
//  RentRoom
//
//  Created by Narith on 10/11/16.
//  Copyright © 2016 praknarith. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PostRoom : NSObject
@property (strong,nonatomic)NSString *uid;
@property(strong,nonatomic)NSString *user;
@property(strong,nonatomic)NSString *location;
@property(strong,nonatomic)NSString *size;
@property(strong,nonatomic)NSString *phone;
@property(strong,nonatomic)NSString *price;

-(instancetype)initWithUid:(NSString *)uid andUser:(NSString *)user andLocation:(NSString *)locaion andSize:(NSString *)size andPhone:(NSString *)phone andPrice:(NSString *)price;
@end
