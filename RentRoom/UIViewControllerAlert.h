//
//  UIViewControllerAlert.h
//  RentRoom
//
//  Created by Narith on 10/12/16.
//  Copyright © 2016 praknarith. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^AlertPromptCompletionBlock)(BOOL userPressedOK, NSString *_Nullable userInput);


@interface UIViewController (Alerts)

- (void)showMessagePrompt:(NSString *)message;

- (void)showTextInputPromptWithMessage:(NSString *)message completionBlock:(AlertPromptCompletionBlock)completion;

@end

NS_ASSUME_NONNULL_END
