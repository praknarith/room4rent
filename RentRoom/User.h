//
//  User.h
//  RentRoom
//
//  Created by Narith on 10/11/16.
//  Copyright © 2016 praknarith. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject
@property(strong,nonatomic) NSString *username;
-(instancetype) initWithUser:(NSString *)username;
@end
