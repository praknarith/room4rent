//
//  PostRoomViewController.m
//  RentRoom
//
//  Created by Narith on 10/10/16.
//  Copyright © 2016 praknarith. All rights reserved.
//

#import "PostRoomViewController.h"
#import "User.h"
#import "PostRoom.h"
#import "UIViewControllerAlert.h"
@import Firebase;

@interface PostRoomViewController () <UITextViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate>


@property (weak, nonatomic) IBOutlet UITextField *sizeTextField;
@property (weak, nonatomic) IBOutlet UITextField *locationTextField;
@property (weak, nonatomic) IBOutlet UITextField *priceTextField;

@end

@implementation PostRoomViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // [START create_database_reference]
    self.ref = [[FIRDatabase database] reference];
    
    
    //set border color & white
    self.sizeTextField.layer.borderWidth=2;
    self.sizeTextField.layer.borderColor=[[UIColor colorWithRed:7.0/255.0 green:199.0/255.0 blue:119.0/255.0 alpha:1.0] CGColor];
    self.locationTextField.layer.borderWidth=2;
    self.locationTextField.layer.borderColor=[[UIColor colorWithRed:7.0/255.0 green:199.0/255.0 blue:119.0/255.0 alpha:1.0] CGColor];
    self.priceTextField.layer.borderWidth=2;
    self.priceTextField.layer.borderColor=[[UIColor colorWithRed:7.0/255.0 green:199.0/255.0 blue:119.0/255.0 alpha:1.0] CGColor];
    //Set textfield focus
//        [self.sizeTextField becomeFirstResponder];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)keyboardDidShow:(NSNotification *)notification
{
    // Assign new frame to view
    [UIView animateWithDuration:0.1 animations:^{
        CGRect frame = self.view.frame;
        frame.origin.y = -100.0f;
        self.view.frame = frame;
    }];
}

-(void)keyboardDidHide:(NSNotification *)notification
{
    [UIView animateWithDuration:0.1 animations:^{
        CGRect frame = self.view.frame;
        frame.origin.y = 0.0f;
        self.view.frame = frame;
    }];
}
- (IBAction)uploadButton:(id)sender {
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.allowsEditing = NO;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        picker.delegate = self;
        [self presentViewController:picker animated:YES completion:nil];
    }

}
-(void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    UIImage *image=info[UIImagePickerControllerOriginalImage];
    self.coverImage.image=image;
    self.coverImage.contentMode=UIViewContentModeScaleAspectFill;
    [picker dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)postButton:(UIBarButtonItem *)sender {
    // [START single_value_read]
    NSString *userId=[FIRAuth auth].currentUser.uid;
    [[[self.ref child:@"user"] child:userId] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        [self newPost:userId location:self.locationTextField.text size:self.sizeTextField.text price:self.priceTextField.text];
       
    } withCancelBlock:^(NSError * _Nonnull error) {
        NSLog(@"%@", error.localizedDescription);
        [self showMessagePrompt:error.localizedDescription];
        return ;
    }];
    //Storage referrence
    FIRStorage *storage = [FIRStorage storage];
    FIRStorageReference *storageRef = [storage referenceForURL:@"gs://room4rent-ca57d.appspot.com"];
    //Upload photo
    NSData *data = UIImagePNGRepresentation(self.coverImage.image);
    NSString *imageName=userId;
    FIRStorageReference *coverRef = [[storageRef  child:@"cover"] child:imageName];
    FIRStorageUploadTask *uploadTask = [coverRef putData:data metadata:nil completion:^(FIRStorageMetadata *metadata, NSError *error) {
        if (error != nil) {
            
        } else {
            // Metadata contains file metadata such as size, content-type, and download URL.
            NSURL *downloadURL = metadata.downloadURL;
        }
    }];
    [self showMessagePrompt:@"Post done!"];

}
- (void)newPost:(NSString *)userID location:(NSString *)location  size:(NSString *)size price:(NSString *)price {
    // Create new post at /$userid/$postid and at
    NSString *key = [[_ref child:@"posts"] childByAutoId].key;
    NSDictionary *post = @{@"uid": userID,
                           @"location": location,
                           @"size": size,
                           @"price": price};
    NSDictionary *childUpdates = @{[@"/posts/" stringByAppendingString:key]: post,
                                   [NSString stringWithFormat:@"/user-posts/%@/%@/", userID, key]: post};
    [_ref updateChildValues:childUpdates];
    // [END write_fan_out]
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}
-(void)clearTextbox{
    self.sizeTextField.text=@"";
    self.locationTextField.text=@"";
    self.priceTextField.text=@"";
}
@end
