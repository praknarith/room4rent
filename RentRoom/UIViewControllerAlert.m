//
//  UIViewControllerAlert.m
//  RentRoom
//
//  Created by Narith on 10/12/16.
//  Copyright © 2016 praknarith. All rights reserved.
//

#import "UIViewControllerAlert.h"

#import <objc/runtime.h>


static NSString *const kOK = @"OK";

static NSString *const kCancel = @"Cancel";

@implementation UIViewController (Alerts)

- (BOOL)supportsAlertController {
    return NSClassFromString(@"UIAlertController") != nil;
}

- (void)showMessagePrompt:(NSString *)message {
    if ([self supportsAlertController]) {
        UIAlertController *alert =
        [UIAlertController alertControllerWithTitle:nil
                                            message:message
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction =
        [UIAlertAction actionWithTitle:kOK style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

-(void)showTextInputPromptWithMessage:(NSString *)message completionBlock:(AlertPromptCompletionBlock)completion{
    if ([self supportsAlertController]) {
        UIAlertController *prompt =
        [UIAlertController alertControllerWithTitle:nil
                                            message:message
                                     preferredStyle:UIAlertControllerStyleAlert];
        __weak UIAlertController *weakPrompt = prompt;
        UIAlertAction *cancelAction =
        [UIAlertAction actionWithTitle:kCancel
                                 style:UIAlertActionStyleCancel
                               handler:^(UIAlertAction * _Nonnull action) {
                                   completion(NO, nil);
                               }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:kOK
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                             UIAlertController *strongPrompt = weakPrompt;
                                                             completion(YES, strongPrompt.textFields[0].text);
                                                         }];
        [prompt addTextFieldWithConfigurationHandler:nil];
        [prompt addAction:cancelAction];
        [prompt addAction:okAction];
        [self presentViewController:prompt animated:YES completion:nil];
    }
}

@end
