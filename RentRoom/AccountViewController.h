//
//  AccountViewController.h
//  RentRoom
//
//  Created by Narith on 10/10/16.
//  Copyright © 2016 praknarith. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>


@interface AccountViewController : UITableViewController
@property (nonatomic, assign) FBSDKProfilePictureMode pictureCropping;
@property (nonatomic, copy) NSString *profileID;

@end
