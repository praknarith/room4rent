//
//  SignupViewController.m
//  RentRoom
//
//  Created by Narith on 9/18/16.
//  Copyright © 2016 praknarith. All rights reserved.
//

#import "SignupViewController.h"
#import "SingInViewController.h"
@import FirebaseAuth;
@import Firebase;

@interface SignupViewController ()

@end

@implementation SignupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //set border color & white
    self.textUser.layer.borderWidth = 2;
    self.textUser.layer.borderColor = [[UIColor colorWithRed:7.0/255.0 green:199.0/255.0 blue:119.0/255.0 alpha:1.0] CGColor];
    self.textSignUpEmail.layer.borderWidth = 2;
    self.textSignUpEmail.layer.borderColor = [[UIColor colorWithRed:7.0/255.0 green:199.0/255.0 blue:119.0/255.0 alpha:1.0] CGColor];
    self.textSignupPassword.layer.borderWidth = 2;
    self.textSignupPassword.layer.borderColor = [[UIColor colorWithRed:7.0/255.0 green:199.0/255.0 blue:119.0/255.0 alpha:1.0] CGColor];
    //Set textfield focus
//    [self.textUser becomeFirstResponder];


    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardWillHideNotification object:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)btnSignUp:(id)sender {
    NSString *email=self.textSignUpEmail.text;
    NSString *password=self.textSignupPassword.text;
    NSString *user=self.textUser.text;
    if(!email.length & !password.length & !user.length){
        return;
    }
    [[FIRAuth auth] createUserWithEmail:email
                               password:password
                             completion:^(FIRUser * _Nullable user, NSError * _Nullable error) {
                                 if (error) {
                                     NSLog(@"%@", error.localizedDescription);
                                     return;
                                 }
                             }];
    NSLog(@"Success");
    
    [self performSegueWithIdentifier:@"signUp" sender:nil];
}

- (IBAction)btnCancel:(id)sender {
    [self performSegueWithIdentifier:@"signUp" sender:nil];
//    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)keyboardDidShow:(NSNotification *)notification
{
    // Assign new frame to view
    [UIView animateWithDuration:0.1 animations:^{
        CGRect frame = self.view.frame;
        frame.origin.y = -100.0f;
        self.view.frame = frame;
    }];
}

-(void)keyboardDidHide:(NSNotification *)notification
{
    [UIView animateWithDuration:0.1 animations:^{
        CGRect frame = self.view.frame;
        frame.origin.y = 0.0f;
        self.view.frame = frame;
    }];
}
@end
