//
//  RoomListViewController.m
//  RentRoom
//
//  Created by Narith on 9/24/16.
//  Copyright © 2016 praknarith. All rights reserved.
//

#import "RoomListViewController.h"
#import "RoomPostDataSource.h"
#import "PostRoom.h"
#import "RoomTableViewCell.h"
#import "RoomDetailViewController.h"

@interface RoomListViewController ()

@end

@implementation RoomListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //Start create database
    self.ref =[[FIRDatabase database] reference];
    self.dataSource=[[RoomPostDataSource alloc] initWithQuery:[self getQuery]
                                                   modelClass:[PostRoom class]
                                                     nibNamed:@"PostRoomViewCell"
                                          cellReuseIdentifier:@"post"
                                                         view:self.tableView];
    
    [self.dataSource populateCellWithBlock:^void(RoomTableViewCell *__nonnull cell, PostRoom *__nonnull post) {
//        cell.profileImage.image=[UIImage imageNamed:@"profile"];
        cell.locationLabel.text=post.location;
        cell.phoneLabel.text=post.phone;
        cell.priceLabel.text=post.price;
//        cell.coverImage.image=[UIImage imageNamed:@"room1"];
    }];
    
    self.tableView.dataSource=self.dataSource;
    self.tableView.delegate=self;
    
    self.tableView.clipsToBounds = NO;
    self.tableView.layer.masksToBounds = NO;
}
- (NSString *) getUid {
    return [FIRAuth auth].currentUser.uid;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(FIRDatabaseQuery *)getQuery{
    return self.ref;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    cell.layer.shadowOpacity = 1.0;
    cell.layer.shadowRadius = 1.7;
    cell.layer.shadowColor = [UIColor blackColor].CGColor;
    cell.layer.shadowOffset = CGSizeMake(0.0, 0.0);
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
        [self performSegueWithIdentifier:@"detail" sender:indexPath];
    
}
- (void)viewWillAppear:(BOOL)animated {
    [self.tableView reloadData];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 200;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSIndexPath *path=sender;
    RoomDetailViewController *detail=segue.destinationViewController;
    FirebaseTableViewDataSource *source=self.dataSource;
    FIRDataSnapshot *snapshot=[source objectAtIndex:path.row];
    detail.postKey=snapshot.key;
}

@end
