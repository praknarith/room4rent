//
//  RoomPostDataSource.m
//  RentRoom
//
//  Created by Narith on 10/11/16.
//  Copyright © 2016 praknarith. All rights reserved.
//

#import "RoomPostDataSource.h"
@import Firebase;

@implementation RoomPostDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([self count] != 0) {
        tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        
        tableView.backgroundView=nil;
    }
    return [self count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
    noDataLabel.text             = @"No post, please wait!";
    noDataLabel.textColor        = [UIColor blackColor];
    noDataLabel.textAlignment    = NSTextAlignmentCenter;
    tableView.backgroundView = noDataLabel;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    return 1;
}

@end
